/* Neste programa trabalha-se com o algoritmo da Mochila B�sica, mostrando
todas as solu��es para um dado valor de mochila.
a) Programe o procedimento SolucaoMochila.
Imprima a solu��o de forma compreensiva, indicando o �ndice do �tem e seu valor.
c) Rode o programa para t = 10, M = 30, 
   �tens = 2, 3, 4, 5, 6, 7, 8, 9, 10, 11
   testando as solu��es para m = 20 , 21, 22,...,30
*/
#include <iostream>
#include <iomanip>
using namespace std;
int i, j, t, M, Ma, P[1001], K[1001], S[1001]; //Ma é o valor desejado da solução

void ImprimeSolucao(int n)
{
	cout << "Sol: ";
	for (int i = 1; i <= n; i++)
		cout << S[i] << " ";
	cout << endl;
}

void SolucaoMochila(int np, int t, int m)
{
	int i, j, ma;
	for (i = t; i >= 1; i-=1)
	{
		ma = m - P[i];
		if (ma >= 0 && K[ma] < i && K[ma] > -1)
		{
			S[np] = i;
			if (ma == 0)
			{
				for (int j = np; j >= 1; j--)
					cout << P[S[j]] << "(" <<S [j] << ") " << " ";
				cout <<endl;
			}
			else
			{
				SolucaoMochila(np + 1, i - 1, ma);
			}
		}
	}
}

void MochilaBasica(int t, int M)
{
	int i, j;
	K[0] = 0;
	for (j = 1; j <= M; j++)
		K[j] = -1;
	for (i = 1; i <= t; i++)
	{
		for (j = M; j >= P[i]; j--)
		{
			if (K[j] == -1 && K[j - P[i]] >= 0)
				K[j] = i;
		}
	}
}

int main()
{
	cout << "t, M = ";
	cin >> t >> M;
	while (true)
	{
		cout << "Informe " << t << " pesos: ";
		for (i = 1; i <= t; i++)
			cin >> P[i];
		MochilaBasica(t, M);
		cout << endl
			 << "Vetor da Mochila para t = " << t << " M = " << M << ":" << endl;
		for (j = 0; j <= M; j++)
			cout << fixed << setw(3) << j;
		cout << endl;
		for (j = 0; j <= M; j++)
			cout << fixed << setw(3) << K[j];
		cout << endl
			 << endl;
		while (true)
		{
			cout << "Solucoes para M = ";
			cin >> Ma;
			if (!Ma)
				break;
			SolucaoMochila(1, t, Ma);
		}
	}
	return 0;
}

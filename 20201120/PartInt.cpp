/*Voc� vai trabalhar com o problema de listar as parti��es distintas
   de um inteiro n.
a) Implementar Part, contando e imprimindo as solu��es distintas.
*/
#include <iostream>
using namespace std;
int S[100], soma, sol, n;

void ImprimeSolucao(int n)
{
    cout << "Sol " << sol << ": ";
    for (int i = 1; i <= n; i++)
        cout << S[i] << " ";
    cout << endl;
}

void Part(int ns, int t)
{
    int i, j;
    i = t;
    while (i <= n && soma + 1 <= n)
    {
        S[ns] = i;
        soma += i;
        if (soma == n)
        {
            sol++;
            ImprimeSolucao(ns);
        }
        else
        {
            Part(ns + 1, i);
        }
        soma -= i;
        i++;
    }

    /*  i <- t
    enquanto (i <=n  e  soma + i <=n):
        S[ns] <- i;  soma <- soma+i;
		se (soma = n):
		    Imprimir e contar
		sen�o:
			Part(ns+1, i)
        soma <- soma-i;
        i++;
*/
}

int main()
{
    while (true)
    {
        cout << endl
             << "n = ";
        cin >> n;
        if (!n)
            break;
        soma = 0;
        Part(1, 1);
        cout << "Numero de solucoes: " << sol << endl;
    }
}

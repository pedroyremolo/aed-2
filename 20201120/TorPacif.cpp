/* Neste programa trabalha-se com uma variante da gera��o de permuta��es, quando
  queremos que uma posi��o da permuta��o seja um valor fixo. Quer-se  a solu��o
  de Torres Pac�ficas, com uma torre fixa em dada posi��o do tabuleiro.
  A id�ia � modificar a gera��o de permuta��es assim:
  Dados l e c, (as coordenadas da posi��o fixa) o algoritmo deve ser modificado
  para, ao se preencher a posi��o c do vetor, esse valor s� poder ser l.
*/
#include <iostream>
using namespace std;
int  P[101], i, l, c, n, sol;
bool S[101];

void Permut(int np, int l, int c){
    int i, j;
    for (i=1; i<=n; i++)
        //if (!S[i] && (np==l && i==c  || np !=l && i !=c)){  
        if (!S[i] && (np==l && i==c  || np !=l && i !=c)){
            P[np]=i;  S[i]=true;
            if (np == n){
                sol++; cout<<sol<<": ";
                for (j=1; j<=n; j++) cout << P[j] <<" "; cout<<endl;
            }
            else Permut(np+1, l, c);
            S[i]=false;
        }
}

int main(){
	cout<<endl<<"n = ";  cin>>n;
    for (i=1; i<=n; i++) S[i]=false;
    while (true){
        cout<<endl<<"Informe l e c (coordenadas da torre fixa): ";
        cin >> l >> c;
        if (l == 0) break;
        cout<<"Solucoes:"<<endl;
        sol=0;  Permut(1, l,c);
    }
    return 0;
}

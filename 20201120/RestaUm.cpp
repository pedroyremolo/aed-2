/*Voc� vai trabalhar com o problema Resta Um em um tabuleiro simplificado P, de
   tamanho n x 1, onde n � informado. Cada posi��o de P � 0(sem pino) ou 1(com pino).
   A situa��o inicial � criada aleatoriamente.
a) Entender o algoritmo e observar que foi duplicado o c�digo para tratar as duas
   diferentes dire��es de movimenta��o dos pinos.
b) Imprima a Configura��o m�nima.
c) Exerc�cio extra: mostre, passo a passo, como foi obtida a comnfigura��o m�nimna.
*/
#include <iostream>
#include <stdlib.h>
using namespace std;
int P[100], M[100], i, n, pmin;

int Inicializa()
{
    int i, j, prest;
    for (i = 1; i <= n; i++)
        P[i] = 0;
    prest = 0;
    for (i = 1; i <= n; i++)
    {
        j = rand() % n + 1;
        if (j < 3 * n / 4)
        {
            P[i] = 1;
            prest++;
        }
    }
    cout << "Configuracao inicial:     ";
    for (i = 1; i <= n; i++)
        cout << P[i];
    cout << endl;
    return prest;
}
void RestaUm(int prest)
{
    int i, j;
    for (i = 1; i <= n - 2; i++)
    {
        if (P[i] == 1 && P[i + 1] == 1 && P[i + 2] == 0)
        {
            P[i] = 0;
            P[i + 1] = 0;
            P[i + 2] = 1;
            prest--;

            if (prest < pmin)
            {
                pmin = prest;
                for (int j = 1; j < n; j++)
                    M[j] = P[j];
            }
            RestaUm(prest);

            P[i] = 1;
            P[i + 1] = 1;
            P[i + 2] = 0;
            prest++;
        }
    }
    for (i = 1; i <= n - 2; i++)
    {
        if (P[i] == 0 && P[i + 1] == 1 && P[i + 2] == 1)
        {
            P[i] = 1;
            P[i + 1] = 0;
            P[i + 2] = 0;
            prest--;

            if (prest < pmin)
            {
                pmin = prest;
                for (int j = 1; j < n; j++)
                    M[j] = P[j];
            }

            RestaUm(prest);

            P[i] = 0;
            P[i + 1] = 1;
            P[i + 2] = 1;
            prest++;
        }
    }
    /*RestaUm:
    para i <- 1..n-2 incl.:
        se P[i] = 1 e P[i+1] = 1 e P[i+2] = 0:
            P[i] <- 0;  P[i+1] <- 0;  P[i+2] <- 1;  prest--;
            se  prest < pmin:
            	pmin <- prest
            	guarda Configura��o m�nima
            RestaUm()
            P[i] <- 1;  P[i+1] <- 1;  P[i+2] <- 0;  prest++;
    para i <- 1..n-2 incl.:
        se P[i] = 0  e  P[i+1] = 1  e  P[i+2]) = 1:
            P[i] <- 1;  P[i+1] <- 0;  P[i+2] <- 0;  prest--;
            se  prest < pmin:
            	pmin <- prest
            	guarda Configura��o m�nima            	
            RestaUm()
            P[i] <- 0;  P[i+1] <- 1;  P[i+2] <- 1;  prest++;
*/
}
int main()
{
    while (true)
    {
        cout << "Informe o tamanho do tabuleiro: ";
        cin >> n;
        if (!n)
            break;
        pmin = Inicializa();
        RestaUm(pmin);
        cout << "Pinos restantes: " << pmin << endl;
        cout << "Configuraao final minima: ";
        for (i = 1; i <= n; i++)
            cout << M[i];
        cout << endl
             << endl;
    }
}

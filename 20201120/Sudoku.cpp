/* Neste programa trabalha-se com o problema Sudoku cl�ssico.
O vetor PL guarda as posi��es iniciais da matriz que n�o est�o preenchidas.
O backtracking tenta preencher essas posi��es de todas as maneiras poss�veis,
baseando-se nos vetors ml, mc, mm, que indica, para cada linha, coluna e mini-
quadrados, os n�meros j� utilizados.
a) Programar o procedimento Solve().
b) Testar com o seguinte dado:

530070000
600195000
098000060
800060003
400803001
700020006
060000280
000419005
000080079

c) Trabalho extra: tente eliminar o maior n�mero poss�vel de posi��es preenchidas
na matriz anterior, de forma a se ter ums �nica solu��o. 
*/
#include <iostream>
using namespace std;
struct cel
{
	int l;
	int c;
	int m;
};
cel PL[100];
int i, j, m, k, nf, sol, ml[10][10], mc[10][10], mm[10][10];
char T[10][10];

void ImprimeSolucao()
{
	cout << endl;
	cout << "Solução " << sol << ":" << endl;
	for (int l = 1; l <= 9; l++)
	{
		for (int c = 1; c <= 9; c++)
		{
			cout << T[l][c] << " ";
		}
		cout << endl;
	}
	cout << endl;
}

void Solve(int nfa)
{
	int i, j;
	cel z = PL[nfa];
	for (i = 1; i <= 9; i++)
	{
		if (ml[z.l][i] == 0 && mc[z.c][i] == 0 && mm[z.m][i] == 0)
		{
			T[z.l][z.c] = char(i + '0');

			ml[z.l][i] = -1;
			mc[z.c][i] = -1;
			mm[z.m][i] = -1;

			if (nfa == nf)
			{
				sol++;
				ImprimeSolucao();
			}
			else
			{
				Solve(nfa + 1);
			}

			T[z.l][z.c] = '0';
			ml[z.l][i] = 0;
			mc[z.c][i] = 0;
			mm[z.m][i] = 0;
		}
	}
	/*  para i<- 1..9 incl.:
		se (ml[z.l][i]=0 e mc[z.c][i]=0 e mm[z.m][i]=0):
      		T[z.l][z.c] <- char(i+'0');  
			ml[z.l][i]<-1; mc[z.c][i]<-1;  mm[z.m][i]<-1;
      		se (nfa = nf):
      			sol++
      			Imprimir solu��o
      		sen�o:
      			Solve(nfa+1)
      		T[z.l][z.c] <- '0';  
			ml[z.l][i]<-0; mc[z.c][i]<-0;  mm[z.m][i]<-0;      			
*/
}

int main()
{
	while (true)
	{
		for (i = 1; i <= 9; i++)
			for (j = 1; j <= 9; j++)
			{
				ml[i][j] = 0;
				mc[i][j] = 0;
				mm[i][j] = 0;
			}
		cout << endl
			 << "Informe a matriz do Sudoku: " << endl;
		nf = 0;
		for (i = 1; i <= 9; i++)
			for (j = 1; j <= 9; j++)
			{
				cin >> T[i][j];
				k = int(T[i][j] - '0');
				m = (((i + 2) / 3) - 1) * 3 + (j + 2) / 3;
				ml[i][k] = 1;
				mc[j][k] = 1;
				mm[m][k] = 1;
				if (k == 0)
				{
					nf++;
					PL[nf].l = i;
					PL[nf].c = j;
					PL[nf].m = m;
				}
			}
		sol = 0;
		Solve(1);
		cout << "Encontrada(s) " << sol << " solucao(oes)." << endl;
	}
	return 0;
}

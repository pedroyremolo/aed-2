/*Neste programa voc� trabalha com o problema de descobrir uma moeda falsa 
  de um conjunto, sabendo que cada moeda normal pesa 2 e a falsa pesa 1.
  A pesagem � feita em uma balan�a digital que informa o peso do conjunto.
  Programe o procedimento ComputadorJoga.
*/
#include <iostream>
#include<stdlib.h>
#include<time.h>
using namespace std;
int n, c, f, p, nth, ntc, ff;

void ComputadorJoga(int c, int f){
    int m, p;
    ntc++;
    if (c == f) {
        cout << "A moeda falsa eh " << c <<". Acertei em " << ntc << " tentativas." << endl;
    } else {
        m = (c+f)/2;
        cout << "Peso da faixa: " << c << " " << m << "(" << m-c+1 << " moedas) : " << endl;
        cin >> p;
        if (p == 2*(m-c+1)) {
            c = m + 1;
        } else {
            f = m;
        }
        ComputadorJoga(c,f);
    }
}

int main()
{	srand(time(NULL));
    ff=20;
    while(true){
	    cout << endl << "Deve ser escolhido um numero entre 1 e "<<ff<<". Primeiro voce adivinha."<<endl;
        n = rand()%ff+1;
        nth=0;
        while(true){
            nth++;
            cout<<"Faixa a pesar: ";
			cin>>c>>f;
            if ((c == f) && (c==n)){
                cout <<"Voce acertou em "<<nth<<" tentativas. "<<endl;
                break;
            }
            cout<<"Peso: ";
            if ((n>=c)&&(n<=f))
				cout<<(f-c+1)*2-1<<endl;
            else  
				cout<<(f-c+1)*2<<endl;             
        } 
	    cout << endl << "Agora eu adivinho. Pense em um numero entre 1 e "<<ff<<". "<<endl;
        ntc=0;  c=1;  f=ff;
        ComputadorJoga(c, f);
        if (ntc> nth) 
			cout<<"Voce ganhou."<<endl;
        else if (ntc < nth) 
			cout<<"Ganhei."<<endl;
        else 
			cout<<"Empate."<<endl;      
	}
	return 0;
}

/*Neste programa voc� vai trabalhar com o problema da Torre de Hanoi.
  Observe que o programa mostra a solu��o do problema, indicando a movimenta��o
  a ser feita. Voc� deve mudar a forma de mostrar o procedimento, indicando apenas,
  a cada passo, o total de pratos de cada vareta.*/
#include <iostream>
using namespace std;

int np, V[4];

void Hanoi(int n, int v1, int v2, int v3){
    if (n > 0) {
        Hanoi(n-1, v1, v3, v2);
        cout <<"Move o topo da vareta " << v1 << " para a vareta " << v3 << "n pratos :"<< n <<endl;
        Hanoi(n-1, v2, v1, v3);
    }
}
  
int main(){
    while(true){
	    cout << endl << "Num. de pratos, 0 para terminar: "; cin >> np;
	    if (np == 0) break;
        Hanoi(np, 1, 2, 3);                 
    }     
	return 0;	
}


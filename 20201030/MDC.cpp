/*O c�lculo do MDC entre dois inteiros a  e  b  pode ser feito atrav�s da 
	seguinte recorr�ncia:  
    
			MDC(a, b) = a, se  b for igual a zero, 
			MDC(a, b) = MDC(b, a mod b), caso contr�rio.
			
    Voc� deve adaptar a recorr�ncia dada para o c�lculo do MDC de 3 inteiros.
*/
#include <iostream>
using namespace std;

int MDC3(int a, int b, int c)
{
}

int main()
{
	int a, b, c, m;
	while (true)
	{
		cout << endl
			 << "Informe a, b e c: ";
		cin >> a >> b >> c;
		if (!a)
			break;
		m = MDC3(a, b, c);
		cout << endl
			 << "MDC(" << a << "," << b << "," << c << ") = " << m << endl;
	}
	return 0;
}

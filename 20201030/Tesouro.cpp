/*Neste programa voc� vai trabalhar com o problema de Ca�a ao Tesouro,
   cuja recorr�ncia foi objeto de um trabalho em sala.
   Dada a matriz M(n x n), determinar o maior valor possivel que se
   recolhe, ao sair da posi��o (1,1) para a posi��o (n,n), usando s�
   caminhos m�nimos (s� andando da coordenada (a,b) para coordenadas
   onde a coordenada a aumenta de 1 ou a coordenada b aumenta de 1.
   A recorr�ncia �:
   
   		T(1,1) = M(1,1)
   		T(1,j) = T(1,j-1) + M(1,j), j > 1
   		T(i,1) = T(i-1,1) + M(i,1), i > 1
   		T(i,j) = max(T(i-1,j), T(i, j-1)+M(i,j), i, j > 1
   		
   e vai comparar as solu��es por Programa��o Din�mica e por Recurs�o com
   Memoriza��o. O preenchimento das matrizes TRP e TRM j� est�o feitos.
   Seu trabalho � imprimir as matrizes e contar o otal de subproblemas resolvidos
   em cada um dos m�todos.
   Programe a fun��o Preenche que preenche a matriz T, usando a recorr�ncia. 
*/
#include<iostream>
#include<stdlib.h>
#include<iomanip>
using namespace std;
int n, i, j, T[101][101], M[101][101], C[101][101];

void MelhorCaminho(int i, int j){
    if (i> 0  && j > 0){
        C[i][j]=1;
        if (i==1){
            MelhorCaminho(1,j-1);
        }
        else if (j==1){
            MelhorCaminho(i-1,1);
        }    
        else if (T[i][j] == T[i-1][j]+M[i][j]){
            MelhorCaminho(i-1, j);
        }
        else{
            MelhorCaminho(i, j-1);
        }    
    }  
}

void Preenche(){
    int i, j;
    
    T[1][1] = M[1][1]; //T(1,1) = M(1,1)

    for(j=2; j<=n; j++) {
        T[1][j] = T[1][j-1] + M[1][j]; //T(1,j) = T(1,j-1) + M(1,j), j > 1
    }
    for(i=2; i<=n; i++) {
        T[1][j] = T[i-1][1] + M[i][1]; //T(i,1) = T(i-1,1) + M(i,1), i > 1
    }
    for(i=2; i<=n; i++) {
        for(j=2; j<=n; j++) {
            T[i][j] = max(T[i-1][j], T[i][j-1]+M[i][j]); //T(i,j) = max(T(i-1,j), T(i, j-1)+M(i,j), i, j > 1
        }
    }
}

int main(){
    n = 8;
    while (true){
        for(i=1; i<=n; i++) for (j=1; j<=n; j++)
		    M[i][j] = rand()%10+1;
		    
		cout<<"Tesouros:"<<endl;
		for(i=1; i<=n; i++){
		    for (j=1; j<=n; j++) cout<<setw(4)<<M[i][j];
		    cout<<endl;		
		}
		
        Preenche();
        cout<<endl<<"Valor recolhido:"<<endl;
		for(i=1; i<=n; i++){
		    for (j=1; j<=n; j++) cout<<setw(4)<<T[i][j];
		    cout<<endl;		
		}
		
		cout<<endl<<"Melhor cminho:"<<endl;
		for(i=1; i<=n; i++) for (j=1; j<=n; j++){
		    C[i][j]=0;
		}
		MelhorCaminho(n, n);
		for(i=1; i<=n; i++){
		    for (j=1; j<=n; j++){ 
				cout<<setw(4)<<C[i][j];
			}
		    cout<<endl;		
		}
		cin.get();
    }
    return 0;
}

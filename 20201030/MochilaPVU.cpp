/*Neste programa voc� vai trabalhar com a Mochila com peso e valor, 
   �tens �nicos. Programe o procedimento MochilaPV, conforme pseudoc�digo.
1. Modifique tamb�m o programa para exibir as transforma��es no vetor 
    ap�s o processamento de cada �tem.
*/
#include <iostream>
#include <stdlib.h>
#include <iomanip>
using namespace std;

struct mochila
{
    int me;
    int vm;
};
mochila K[1000001];

struct item
{
    int p;
    int v;
};
item O[1000];

int M, t, j, np, soma;

void MochilaPV()
{
    int i, j;
    K[0].me = 0;
    K[0].vm = 0;
    for (j = 1; j <= M; j++)
    {
        K[j].me = -1;
        K[j].vm = 0;
    }
    for (i = 1; i <= t; i++)
    {
        for (j = M; j >= O[i].p; j--)
        {
            if (K[j - O[i].p].me >= 0 && K[j].vm < K[j - O[i].p].vm + O[i].v)
            {
                K[j].me = i;
                K[j].vm = K[j - O[i].p].vm + O[i].v;
            }
        }
    }
    /*  K[0].me = 0;  K[0].vm = 0;
    para j <-1..m incl: 
		K[j].me = -1
		K[j].vm = 0
    para i <- 1..t incl.:
        para j <-  m.. O[i].p incl. passo -1:
	        se (K[j-O[i].p].me >= 0   e   K[j].vm < (K[j-O[i].p].vm+O[i].v)):
		        K[j].me <- i
				K[j].vm <- K[j-O[i].p].vm+O[i].v 
*/
}
int main()
{
    while (true)
    {
        cout << endl
             << "t M = ";
        cin >> t >> M;
        if (M == 0)
            break;

        cout << endl
             << "Pesos/valores gerados: ";
        for (j = 1; j <= t; j++)
        {
            O[j].p = rand() % 4 + 1;
            O[j].v = rand() % 6 + 1;
            cout << O[j].p << "/" << O[j].v << "   ";
        }

        cout << endl
             << endl;
        for (j = 0; j <= M; j++)
            cout << setw(6) << j;
        cout << endl;
        MochilaPV();
        for (j = 0; j <= M; j++)
            cout << setw(3) << K[j].me << "/" << setw(2) << K[j].me;
        cout << endl;
    }
    return 0;
}

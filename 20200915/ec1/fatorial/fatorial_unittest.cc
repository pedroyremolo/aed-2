#include "fatorial.h"
#include "gtest/gtest.h"

namespace
{
    GTEST_TEST(FatorialTest, SucessoPositivo)
    {
        unsigned long long expected_results[2] = {3628800, 2432902008176640000};
        for (int i = 0; i < 1; i++)
        {
            int chamadas = 0;
            ASSERT_EQ(expected_results[i], Fatorial((i+1)*10, &chamadas));
        }
    }

    TEST(FatorialTest, SucessoZero)
    {
        int chamadas = 0;
        ASSERT_EQ(1, Fatorial(0, &chamadas));
        ASSERT_EQ(1, chamadas);
    }
} // namespace

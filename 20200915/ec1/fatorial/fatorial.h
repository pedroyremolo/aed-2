#ifndef EC1_FATORIAL_H_
#define EC1_FATORIAL_H_

// Retorna n! (i.e n fatorial). Para n negativo ou 0, o resultado será 1
unsigned long long int Fatorial(unsigned long long int, int*);

#endif // EC1_FATORIAL_H_
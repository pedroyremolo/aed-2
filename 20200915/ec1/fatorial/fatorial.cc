#include "fatorial.h"
#include <iostream>

using namespace std;

unsigned long long int Fatorial(unsigned long long int n, int *chamadas)
{
    (*chamadas)++;
    if (n <= 0)
        return 1;
    return n * Fatorial(n - 1, chamadas);
}
#include <iostream>

using namespace std;

unsigned long long Fibonacci(unsigned long long n, unsigned long long *chamadas)
{
    (*chamadas)++;
    if (n <= 1)
        return n;
    return Fibonacci(n - 1, chamadas) + Fibonacci(n - 2, chamadas);
}
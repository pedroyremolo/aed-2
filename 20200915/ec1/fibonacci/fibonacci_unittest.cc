#include "fibonacci.h"
#include "gtest/gtest.h"

namespace
{
    GTEST_TEST(FibonacciTest, SucessoPositivo)
    {
        unsigned long long expected_results[2] = {55, 6765};
        for (int i = 0; i < 1; i++)
        {
            unsigned long long chamadas = 0;
            ASSERT_EQ(expected_results[i], Fibonacci((i+1)*10, &chamadas));
        }
    }

    GTEST_TEST(FibonacciTest, SucessoZero)
    {
        unsigned long long chamadas = 0;
        ASSERT_EQ(0, Fibonacci(0, &chamadas));
        ASSERT_EQ(1, chamadas);
    }
} // namespace

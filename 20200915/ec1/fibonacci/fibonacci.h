#ifndef EC1_FIBONACCI_H_
#define EC1_FIBONACCI_H_

// Retorna o valor n-ésimo termo de fibonacci.
unsigned long long Fibonacci(unsigned long long n, unsigned long long* chamadas);

#endif // EC1_FIBONACCI_H_
#include "fatorial/fatorial.h"
#include "fibonacci/fibonacci.h"
#include <iostream>

using namespace std;

int main(int argc, char const *argv[])
{
    cout << "------------------FATORIAL-------------------" << endl;
    cout << "|         N          |       Resultado        |       Número de chamadas        |" << endl;
    cout << "|--------------------|------------------------|---------------------------------|" << endl;
    for (int i = 0; i <= 100; i+=10)
    {
        int chamadas = 0;
        unsigned long long result = Fatorial(i, &chamadas);
        cout << "|"<< i << "|" << result << "|" << chamadas << "|" << endl;
    }
    cout << "------------------FIM FATORIAL-------------------" << endl;

    cout << "------------------FIBONACCI-------------------" << endl;
    cout << "|         N          |       Resultado        |       Número de chamadas        |" << endl;
    cout << "|--------------------|------------------------|---------------------------------|" << endl;
    for (int i = 0; i <= 100; i+=10)
    {
        unsigned long long chamadas = 0;
        unsigned long long result = Fibonacci(i, &chamadas);
        cout << "|"<< i << "|" << result << "|" << chamadas << "|" << endl;
    }
    cout << "------------------FIM FIBONACCI-------------------" << endl;
    return 0;
}

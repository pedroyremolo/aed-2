# Relatório EC1

## Fatorial

- Algoritmo

  ```
  int Fatorial(p):
      if p ≤ 0:
          return 1
      else:
          return  p * Fatorial(p-1)
  ```

---

- Resultados

| N   | Resultado            | Número de chamadas |
| --- | -------------------- | ------------------ |
| 0   | 1                    | 1                  |
| 10  | 3628800              | 11                 |
| 20  | 2432902008176640000  | 21                 |
| 30  | 9682165104862298112  | 31                 |
| 40  | 18376134811363311616 | 41                 |
| 50  | 15188249005818642432 | 51                 |
| 60  | 9727775195120271360  | 61                 |
| 70  | 0                    | 71                 |
| 80  | 0                    | 81                 |
| 90  | 0                    | 91                 |
| 100 | 0                    | 101                |

---

- Conclusões

  Primeiramente, cabe esclarecer que, por limitações da ferramenta, o cálculo do fatorial utilizando o algoritmo proposto se limita a valores de resultados menores que 2^64. Por isso, a partir do resultado N = 30, os valores apresentados, apesar de retornados pelo programa, são incorretos.

  Além disso, é visto que o número de chamadas cresce linearmente (n+1), levando a crer que é uma boa solução para o problema fatorial.

## Fibonacci

- Algoritmo

  ```
  int Fibonacci(p):
      if p ≤ 1:
          return 1
      else:
          return  Fibonacci(p-1) + Fibonacci(p-2)
  ```

- Resultados

  | N   | Resultado   | Número de chamadas |
  | --- | ----------- | ------------------ |
  | 0   | 0           | 1                  |
  | 10  | 55          | 177                |
  | 20  | 6765        | 21891              |
  | 30  | 832040      | 2692537            |
  | 40  | 102334155   | 331160281          |
  | 50  | 12586269025 | 40730022147        |
  | 60  | ?           | ?                  |
  | 70  | ?           | ?                  |
  | 80  | ?           | ?                  |
  | 90  | ?           | ?                  |
  | 100 | ?           | ?                  |

- Conclusões

    Ao observarmos a tabela de resultados e a imagem abaixo, chega-se a conclusão de que o número de chamadas cresce de maneira exponencial. Não obstante, existe um desperdício computacional na execução do algoritmo, de modo que o mesmo realiza o mesmo cálculo repetidas vezes. Dessa forma, a solução recursiva de fibonacci não é viável. 

    ![Árvore de chamadas Fibonacci(3, 0)](assets/images/fibo.jpg)

    Tal fato se comprova quando ao realizarmos a chamada da função para valores acima de 50, o tempo de retorno é impraticável.
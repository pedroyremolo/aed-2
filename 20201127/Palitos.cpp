/*Voc� vai trabalhar com o algoritmo para o Jogo de Palitos.
a) Primeiramente entenda e veja que n�o foi implementada qualquer poda.
   Determine experimentalmente para at� que valores o programa roda razoavelmente.
   Esse valor � igual a: 32
   
b) Coloque a poda de valor limite e verifique que agora o programa consegue
   rodar para mais palitos. Mas o ideal seria colocar mais podas...
   O novo valor � igual a: 45
     
c) Veja que o programa est� permitindo que o advers�rio retire mais de 3 palitos.
   Corrija isso.
*/
#include <iostream>
using namespace std;
struct retorno
{
    int valor;
    int np;
};
const int MAX_JOGADA = 3;
int npt, npret;
retorno r;

retorno Avalia(int n, string modo)
{
    int i, npm, valorm;
    retorno ret;
    if (n == 0)
    {
        if (modo == "MAX")
            ret.valor = -1;
        else
            ret.valor = 1;
        ret.np = 0;
        return ret;
    }
    else
    {
        if (modo == "MAX")
        {
            valorm = -2;
            for (i = 1; i <= min(3, n); i++)
            {
                ret = Avalia(n - i, "MIN");
                if (ret.valor == 1) {
                    ret.np = i;
                    return ret;
                }
                /*  Poda de valor limite
				se (ret.valor = 1){
				    ret.np <- i
                    retornar ret
                }
                */
                if (ret.valor > valorm)
                {
                    valorm = ret.valor;
                    npm = i;
                }
            }
        }
        else
        {
            valorm = 2;
            for (i = 1; i <= min(3, n); i++)
            {
                ret = Avalia(n - i, "MAX");
                if (ret.valor == -1) {
                    ret.np = i;
                    return ret;
                }
                /*  Poda de valor limite
                se (ret.valor = -1){
				    ret.np <- i                
                    retornar ret
                }
                */
                if (ret.valor < valorm)
                {
                    valorm = ret.valor;
                    npm = i;
                }
            }
        }
        ret.valor = valorm;
        ret.np = npm;
        return ret;
    }
}
void Comeca()
{
    cout << endl
         << "Num. de palitos: ";
    cin >> npt;
    cout << endl;
}
void ImprPal(int n)
{
    cout << "Palitos restantes (" << n << "): ";
    for (int i = 1; i <= n; i++)
        cout << '|';
    cout << endl;
}

void GetJogadaAdversario()
{
    cout << endl
         << "Retira quantos palitos? ";
    cin >> npret;
}

int main()
{
    Comeca();
    while (true)
    {
        ImprPal(npt);
        r = Avalia(npt, "MAX");
        npt = npt - r.np;
        cout << endl
             << "Retiro " << r.np << " palito(s). ";
        ImprPal(npt);
        if (npt == 0)
            cout << "Fim de jogo. Ganhei." << endl;
        else
        {
            GetJogadaAdversario();
            while (npret > MAX_JOGADA) {
                cout << endl << npret << " é maior que " << MAX_JOGADA << ", que é o máximo permitido. Faça uma nova jogada";
                GetJogadaAdversario();
            }
            npt = npt - npret;
            if (npt == 0)
                cout << "Fim de jogo. Voce ganhou." << endl;
        }
        if (npt == 0)
            Comeca();
    }
}

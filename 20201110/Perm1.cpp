/*Neste programa  voc�  vai  trabalhar  com a  gera��o  de  Permuta��es
   Implementar o fun��o Perm(),  cujo pseudoc�digo  � dado. Ao imprimir
   as permuta��es, n�o se esque�a de separar com um branco os elementos 
   da permuta��o.
*/
#include <iostream>
using namespace std;
int n, i, P[50];
bool S[50];

void escreve(int *array)
{
	cout << "P: ";
	for (int i = 1; i <= n; i++)
	{
		cout << array[i] << " ";
	}
	cout << endl;
}

void Perm(int np) // np é o cursor para iterar sobre P
{
	int i, j;
	for (i = 1; i <= n; i++)
	{
		if (!S[i])
		{
			P[np] = i;
			S[i] = true;
			if (np == n)
			{
				escreve(P);
			} else
			{
				Perm(np + 1);
			}
			S[i] = false;
		}
	}

	/*  para i <- 1..n inclusive:
		se (~S[i]):
			P[np] <- i
			S[i] <- verdadeiro
			se (np = n):
				escreve (P)
			sen�o:
				Perm(np+1)
			S[i] <- falso
*/
}

int main()
{
	while (true)
	{
		cout << " n = ";
		cin >> n;
		for (i = 1; i <= n; i++)
			S[i] = false;
		cout << "Permuta��es de " << n << ":" << endl;
		Perm(1);
	}
	return 0;
}

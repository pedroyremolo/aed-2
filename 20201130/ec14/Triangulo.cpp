/* 
Dado um número n, maior que 3, escrever todos os conjuntos distintos de 3 números compreendidos entre 2 e n que podem formar lados de um triângulo.
Ex: Para n = 5, temos:
        2 3 4
        2 4 5
        3 4 5
*/

#include <iostream>
#include <string.h>
using namespace std;

const int N_LADOS_TRIANGULO = 3, TAM_MIN_LADO = 2;
int tamMaxLado, P[4], numTriangulos;

/**
 * Retorna se é possível formar um triangulo com os lados
 * 'a', 'b' e 'c'.
 * 
 * @param a: O menor lado do triangulo
 * @param b: O segundo menor lado do triangulo
 * @param c: O maior lado do triangulo
 **/ 
bool formaTriangulo(int a, int b, int c)
{
    return (a + b) > c;
}

/**
 * Escreve e conta o número de combinações possíveis de
 * triângulos com tamanho máximo tamMaxLado.
 * 
 * @param np: Sentinela que controla o número de posições preenchidas da solução.
 * @param t: Ponto inicial de tentativa da combinação.
 */ 
void geraCombTriangulo(int np, int t)
{
    for (int i = t; np <= N_LADOS_TRIANGULO && i <= tamMaxLado; i++)
    {
        P[np] = i;
        if (np == N_LADOS_TRIANGULO && formaTriangulo(P[np - 2], P[np - 1], i))
        {
            for (int j = 1; j <= np; j++)
                cout << P[j] << " ";
            cout << endl;
            numTriangulos++;
        }
        else
        {
            geraCombTriangulo(np + 1, i + 1);
        }
    }
}

int main(int argc, char const *argv[])
{
    cout << "Insira o tamanho máximo do lado do triangulo:" << endl;
    cin >> tamMaxLado;
    if (tamMaxLado <= TAM_MIN_LADO + 1) {
        cout << tamMaxLado << " não é uma entrada permitida."<< "Tente novamente e insira um número maior que " << TAM_MIN_LADO + 1 << endl;
        return 1;
    }

    numTriangulos = 0;
    geraCombTriangulo(1, TAM_MIN_LADO);

    cout << "Existem " << numTriangulos << " combinações possíveis de triangulos \ncom lado de tamanho máximo " << tamMaxLado;
}